import 'package:email_otp/email_otp.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'MailSend.dart';
import 'Screens/Talent/ProfilMission/Profil1.dart';


class Otp extends StatelessWidget {
  const Otp({
    Key? key,
    required this.otpController,
  }) : super(key: key);
  final TextEditingController otpController;

  @override
  Widget build(BuildContext context) {

    return SizedBox(
      width: 20,
      height: 50,
      child: TextFormField(
        controller: otpController,
        keyboardType: TextInputType.number,
        style: Theme.of(context).textTheme.headline6,
        textAlign: TextAlign.center,
        inputFormatters: [
          LengthLimitingTextInputFormatter(1),
          FilteringTextInputFormatter.digitsOnly
        ],
        onChanged: (value) {
          if (value.length == 1) {
            FocusScope.of(context).nextFocus();
          }
          if (value.isEmpty) {
            FocusScope.of(context).previousFocus();
          }
        },
        decoration: const InputDecoration(
          hintText: ('0'),
        ),
        onSaved: (value) {},
      ),
    );
  }
}

class OtpScreen extends StatefulWidget {
  const OtpScreen({Key? key,required this.myauth}) : super(key: key);
  final EmailOTP myauth ;

  @override
  State<OtpScreen> createState() => _OtpScreenState();
}

class _OtpScreenState extends State<OtpScreen> {
  TextEditingController otp1Controller = TextEditingController();
  TextEditingController otp2Controller = TextEditingController();
  TextEditingController otp3Controller = TextEditingController();
  TextEditingController otp4Controller = TextEditingController();
  Color myColor = Color.fromRGBO(237, 28, 36, 1.0);
  String otpController = "1234";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: myColor,
        leading: IconButton(
          onPressed: () {
            Navigator.push(
                context,
                PageRouteBuilder(
                    pageBuilder: (_, __, ___) => MailSend()
                )
            );
          },
          icon: const Icon(Icons.arrow_back_ios_new),
        ),
      ),
      body: Column(


        children: [
          Padding(padding: EdgeInsets.symmetric(vertical: 5,
              horizontal: 25)),
          SizedBox(
            height: 15,
          ),
          Image.network('https://cdn.dribbble.com/users/2892587/screenshots/14374387/media/0da46661df48f9cf623317f19303d9c6.gif',
            height: 90,
            width: 90,),

          RichText(text: TextSpan(
            text: 'Consulter vos mails\n',
            style: TextStyle(
              fontSize: 30.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),),
          RichText(text: TextSpan(
            text: 'Nous avons envoyé un code de validation ' ,
            style: TextStyle(
              color: Colors.black,
            ),
          ),),
          RichText(text: TextSpan(
            text: ' dans votre courriel pour finaliser la ',
            style: TextStyle(
              color: Colors.black,
            ),
          ),),
          RichText(text: TextSpan(
            text: 'création de votre compte',
            style: TextStyle(
              color: Colors.black,
            ),
          ),),
          SizedBox(
            height: 5,),
          MouseRegion(
            cursor: SystemMouseCursors.click,
            child: GestureDetector(
              onTap: () {
    Navigator.push(
    context,
    PageRouteBuilder(
    pageBuilder: (_, __, ___) => MailSend()
    )
    );
    },
                // votre action lorsque l'utilisateur clique sur le lie
              child: Text(
                "Modifier votre adresse email ?",
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 25,
          ),



          //---------------------------------

          const Icon(Icons.dialpad_rounded, size: 30),
          const SizedBox(
            height: 30,
          ),

          const Text(
            "Veuillez saisir le code de vérification!",
            style: TextStyle(fontSize: 20),

          ),
          const SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Otp(
                otpController: otp1Controller,
              ),
              Otp(
                otpController: otp2Controller,
              ),
              Otp(
                otpController: otp3Controller,
              ),
              Otp(
                otpController: otp4Controller,
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          MouseRegion(
            cursor: SystemMouseCursors.click,
            child: GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    PageRouteBuilder(
                        pageBuilder: (_, __, ___) => MailSend()
                    )
                );
              },
              // votre action lorsque l'utilisateur clique sur le lie
              child: Text(
                "Je trouve pas le code !",
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 160,
          ),
          ElevatedButton.icon(
            style: ButtonStyle(
    backgroundColor: MaterialStatePropertyAll(myColor) ,
    padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 128, vertical: 10),),
    iconSize: MaterialStateProperty.all(20),
    ),
            onPressed: () async {

              if (await widget.myauth.verifyOTP(otp: otp1Controller.text +
                  otp2Controller.text +
                  otp3Controller.text +
                  otp4Controller.text) == true) {
                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text("Code vérifié",style: TextStyle(color: Colors.white,fontSize: 15,fontWeight: FontWeight.bold, )),
                  backgroundColor: Colors.green,
                ));
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const Profil1()));
              }
              else {
               ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                  content: Text("Code invalide",style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold, )),
                 backgroundColor: Colors.redAccent,
                ));
              }
            },
            label: Text(
              "Confirmer",
              style: TextStyle(fontSize: 20),
            ),
            icon: Icon(Icons.check),
          )
        ],
      ),
    );
  }
}