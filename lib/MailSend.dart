import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:email_otp/email_otp.dart';
import 'Screens/Talent/Compte/ConfirmationDialog.dart';
import 'send_otp.dart';
import 'package:flutter/material.dart';

class MailSend extends StatefulWidget {
  const MailSend({Key? key}) : super(key: key);

  @override
  State<MailSend> createState() => _MailSendState();
}
int itemCount =0;

final imageUrls = [
  'https://img.freepik.com/free-vector/character-illustration-people-with-internet-message-icons_53876-59877.jpg?w=900&t=st=1684354432~exp=1684355032~hmac=48d3c88a61a8a75c9572530ff81bc394346cf9b11c17b6b0b1481b60592b3f4c',
  'https://img.freepik.com/free-vector/speech-bubble-3d-icon-message-concept-realistic-speech-bubble-with-dots-chat-vector-message-box_90220-978.jpg?w=740&t=st=1684353923~exp=1684354523~hmac=3bc6e0aa55c36c4e75e772d6651fccb1c1be79fb38441766fae133a62ddeacc1',
  'https://img.freepik.com/vecteurs-libre/conception-plate-dessinee-main-rassemblant-donnees-dans-concept-entreprise_23-2149149219.jpg?w=360',
  'https://st.depositphotos.com/57803962/57677/v/600/depositphotos_576776212-stock-illustration-mailbox-with-mail-icon-vector.jpg',
];
Color myColor = Color.fromRGBO(237, 28, 36, 1.0);
class _MailSendState extends State<MailSend> {
  TextEditingController email = TextEditingController();
  EmailOTP myauth = EmailOTP();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: myColor,
        leading: IconButton(
          onPressed: () {
            Navigator.push(
                context,
                PageRouteBuilder(
                    pageBuilder: (_, __, ___) => ConfirmationDialog()
                )
            );
          },
          icon: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
              child:CarouselSlider.builder(
                itemCount: imageUrls.length,
                options: CarouselOptions(
                height: 200,
                autoPlayInterval: Duration(seconds: 3),
                scrollDirection: Axis.horizontal,
                autoPlayAnimationDuration: Duration(milliseconds: 800),
                autoPlayCurve: Curves.fastOutSlowIn,
                enlargeCenterPage: true,
                autoPlay: true,),
                itemBuilder:  (context, index, realIndex) {
              return Padding(padding: const EdgeInsets.symmetric(horizontal: 40),
                child: SizedBox(
                  height: 400,
                  width: double.infinity,
                  child:Image.network(
                    imageUrls[index % imageUrls.length],
                    fit: BoxFit.cover,
                  ) ,
                ));
          }),
      ),
          const SizedBox(
            height: 60,
            child: Text(
              "Entrer votre adresse mail",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
            ),
          ),
          Card(
            child: Column(
              children: [
                TextFormField(
                  controller: email,
                  decoration: InputDecoration(
                    prefixIcon: const Icon(
                      Icons.mail,
                    ),
                    suffixIcon: IconButton(
                        onPressed: () async {
                          myauth.setConfig(
                              appEmail: "contact@gmail.com",
                              appName: "Votre code de validation",
                              userEmail: email.text,
                              otpLength: 4,
                              otpType: OTPType.digitsOnly);
                          if (await myauth.sendOTP() == true) {
                            ScaffoldMessenger.of(context)
                                .showSnackBar(const SnackBar(
                              content: Text("Code de validation envoyé"),
                              backgroundColor: Colors.green,
                            ));
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>   OtpScreen(myauth: myauth,)));
                          }
                          else {
                            ScaffoldMessenger.of(context)
                                .showSnackBar(const SnackBar(
                              content: Text("Oops, email incorrect"),
                              backgroundColor: Colors.redAccent,
                            ));
                          }
                        },
                        icon: const Icon(
                          Icons.send_rounded,
                          color: Colors.grey,
                        )),
                    hintText: "Adresse mail",
                    border: const OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15.0)),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
