import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:projettest/Screens/Talent/ProfilMission/Cmission3.dart';
import 'package:projettest/Screens/Talent/ProfilMission/E_connexion.dart';
import 'NotExperience.dart';

class Cmission extends StatefulWidget {
  const Cmission({super.key});

  @override
  State<Cmission> createState() => _CmissionState();
}

class _CmissionState extends State<Cmission> {
  @override
  final _formkey = GlobalKey<FormState>();
  // final RegExp emailRegex = RegExp(r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b');
  // String _email = "";

  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: Padding (padding: EdgeInsets.all(25),
              child: Column (
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(

                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: Text(
                          " <",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,

                          ),
                        ),
                      ),
                      SizedBox(width:15,),


                      Expanded(
                          child:Container(
                            height: 5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Color.fromARGB(255, 228, 225, 225),
                            ),
                            child: LinearProgressIndicator(
                              value: 0.2,
                              backgroundColor: Colors.transparent,
                              valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),

                            ),
                          )
                      )



                    ],
                  ),


                  SizedBox(height: 20,),

                  Text("Quel est l'adresse  de\n votre domicile",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),),



                  SizedBox(height: 15),

                  TextFormField(


                    decoration: InputDecoration(
                      hintText: 'Ex :61 rue de Boudonville,54000,Nancy',
                      prefixIcon: Icon(Icons.home, color: Colors.grey,),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0.0),
                        borderSide: BorderSide  (color: Colors.grey,),

                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(0.0),
                        borderSide: BorderSide  (color: Colors.grey,),

                      ),

                    ),
                  ),
                  SizedBox(
                    height: 200,
                  ),


                  Center(


                    child:
                    Container(
                      width:300,
                      height: 70,

                      child: ElevatedButton(

                        onPressed
                            :() {
                          if (_formkey.currentState != null){
                            _formkey.currentState!.validate();
                            // print(_email);
                          }
                          showDialog(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(

                                content: Column(

                                  mainAxisSize: MainAxisSize.min,
                                  children: [

                                    Image.network(
                                      'https://www.hecexecutiveschool.be/hubfs/Article%20de%20blog%20-%20Sortez%20vos%20m%C3%A9diations%20de%20lorni%C3%A8re.jpg',
                                      width: 200,
                                      height: 200,
                                    ),
                                    SizedBox(height: 15),

                                    Text("Pouvez-vous travailler en France"),
                                    SizedBox(height: 15),
                                    Text("Pour accepter des missions sur Seven Job vous\n         devez certifier que vous êtes autorisé à\n                          travailler en France"),
                                    SizedBox(height: 15),
                                    Center(


                                      child:
                                      Container(
                                        width:340,
                                        height: 55,

                                        child: ElevatedButton(
                                          onPressed: () {
                                            Navigator.push(
                                              context,
                                              MaterialPageRoute(builder: (context) => Cmission3()),
                                            );
                                          },
                                          style: ElevatedButton.styleFrom(
                                            elevation: 0,
                                            padding: EdgeInsets.symmetric(vertical: 15),
                                            primary: Color.fromARGB(255, 230, 154, 39),

                                          ),

                                          child: Text('Je peux travailler en France',
                                            style: TextStyle(
                                                fontSize: 7
                                            ),),

                                        ),
                                      ),

                                    ),
                                    SizedBox(height: 15),
                                    Center(


                                      child:
                                      Container(
                                        width:340,
                                        height: 35,

                                        child: ElevatedButton(
                                          onPressed: () {
                                            Navigator.push(
                                                context,
                                            PageRouteBuilder(
                                              //je teste juste que le redirection marche

                                                pageBuilder: (_, __, ___) => NotExperience()
                                            ),
                                            );
                                            },
                                          style: ElevatedButton.styleFrom(

                                              padding: EdgeInsets.symmetric(vertical: 15),
                                              primary:Theme.of(context).dialogBackgroundColor ,
                                              onPrimary: Colors.black

                                          ),

                                          child: Text('Je ne peux pas travailler en France',
                                            style: TextStyle(
                                                fontSize: 17
                                            ),),

                                        ),
                                      ),

                                    ),
                                    SizedBox(height: 15),


                                  ],
                                ),

                              );
                            },
                          );

                        },
                        style: ElevatedButton.styleFrom(
                          elevation: 0,
                          padding: EdgeInsets.symmetric(vertical: 15),
                          primary: Color.fromARGB(255, 230, 154, 39),

                        ),

                        child: Text('Suivant',
                          style: TextStyle(
                              fontSize: 17
                          ),),

                      ),
                    ),

                  ),









                ],
              ),
            )));

  }
}