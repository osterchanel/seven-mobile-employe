import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:projettest/Screens/Talent/ProfilMission/InputWrapper.dart';


class Profil1 extends StatefulWidget {
  const Profil1({super.key});

  @override
  State<Profil1> createState() => _Profil1State();
}
Color myColor = Color.fromRGBO(237, 28, 36, 1.0);
class _Profil1State extends State<Profil1> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: Padding (padding: EdgeInsets.all(10),
            child: Center(
                child: SingleChildScrollView(
                    child: Column (
                      children: [
                        Text("Vous allez créer votre profil",
                          style: TextStyle(
                              fontSize: 30.0,
                              fontWeight: FontWeight.bold
                          ),),
                        SizedBox(
                          height: 20.0,
                        ),
                        Text('Cette étape est indispensable pour recevoir des missions.', style: TextStyle(
                            fontSize: 15.0
                        ),),
                        Text("Vous pouvez quitter et reprendre quand vous le souhaitez.", style: TextStyle(
                            fontSize: 15.0, height: 1.5
                        ),),
                        Text("(vos réponses seront enregistrées à chaque étape)", style: TextStyle(
                            fontSize: 15.0, height: 1.5
                        ),),

                        SizedBox(height: 50,),
                        Image.network('https://cdn5.coloritou.com/dessins/peindre/202244/horloge-ancienne-la-maison-127258.jpg',
                          height: 150,
                          width: 150,),
                        Text('Prévoyez 5 minutes'),

                        SizedBox(height: 200),

                        SizedBox(
                            width: 400, // largeur du boutonE
                            child: ElevatedButton(
                              onPressed:(){
                                Navigator.push(
                                    context,
                                    PageRouteBuilder(
                                      //je teste juste que le redirection marche

                                        pageBuilder: (_, __, ___) => InputWrapper()
                                    )
                                );
                              }
                              ,

                              child: Text("D'accord",
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold
                                ),),
                              style: ElevatedButton.styleFrom(
                                padding: EdgeInsets.symmetric(horizontal:20, vertical: 15.0),
                                primary: Color.fromARGB(255, 237, 28, 36),
                              ),)
                        ),


                      ],)


                )
            ),
          ),

        )
    );
  }

  send_otp() {}
}
