import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:projettest/Screens/Talent/ProfilMission/Cmission4.dart';

class Cmission3 extends StatefulWidget {
  const Cmission3({super.key});

  @override
  State<Cmission3> createState() => _Cmission3State();
}

class _Cmission3State extends State<Cmission3> {
  @override
  List <bool> _isChecked = [false,false];
  bool isAnyChecked() {
    for (var i = 0; i < _isChecked.length; i++) {
      if (_isChecked[i]) {
        return true;
      }
    }
    return false;
  }

  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: Padding (padding: EdgeInsets.all(25),
              child: Column (
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(

                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: Text(
                          " <",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,

                          ),
                        ),
                      ),
                      SizedBox(width:15,),


                      Expanded(
                          child:Container(
                            height: 5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Color.fromARGB(255, 228, 225, 225),
                            ),
                            child: LinearProgressIndicator(
                              value: 0.3,
                              backgroundColor: Colors.transparent,
                              valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),

                            ),
                          )
                      )



                    ],
                  ),


                  SizedBox(height: 20,),

                  Text("Quand seriez-vous disponible\npour une mission",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),),

                  SizedBox(height: 40,),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Column(
                        children: [
                          Container(
                            height: 50,

                            decoration: BoxDecoration(
                              border: Border.all(color: Color.fromARGB(255, 218, 218, 218), width: 1.0),
                              borderRadius: BorderRadius.circular(10),

                            ),
                            child: Row(
                              children: [
                                Padding(padding: EdgeInsets.only(left: 10,)),
                                Checkbox(

                                  value: _isChecked[0],
                                  onChanged: (bool? value) {
                                    setState(() {
                                      _isChecked[0]= value!;
                                    });
                                  },

                                  activeColor: Colors.orange,
                                  shape: CircleBorder(),
                                ),

                                SizedBox(width : 7),


                                Text("Dès maintenant",
                                  style: TextStyle(

                                    fontSize: 15,
                                  ),)
                              ],
                            ),
                          )
                        ],
                      ) ,


                    ],

                  ),
                  SizedBox(height: 12,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Column(
                        children: [
                          Container(
                            height: 50,


                            decoration: BoxDecoration(
                              border: Border.all(color: Color.fromARGB(255, 218, 218, 218), width: 1.0),
                              borderRadius: BorderRadius.circular(10),

                            ),
                            child: Row(
                              children: [
                                Padding(padding: EdgeInsets.only(left: 10,)),
                                Checkbox(

                                  value: _isChecked [1],
                                  onChanged: (bool? value) {
                                    setState(() {
                                      _isChecked[1] = value!;
                                    });
                                  },

                                  activeColor: Colors.red[700],
                                  shape: CircleBorder(),
                                ),

                                SizedBox(width : 7),


                                Text("Dans quelques semaines ou mois",
                                  style: TextStyle(

                                    fontSize: 15,
                                  ),)
                              ],
                            ),
                          )
                        ],
                      ) ,
                      SizedBox(height: 250,),

                      if (isAnyChecked())
                        Center(


                          child:
                          Container(
                            width:300,
                            height: 75,

                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.push(
                                    context,
                                    PageRouteBuilder(
                                      //je teste juste que le redirection marche

                                        pageBuilder: (_, __, ___) => Cmission4()
                                    )
                                );
                              },
                              style: ElevatedButton.styleFrom(
                                elevation: 0,
                                padding: EdgeInsets.symmetric(vertical: 15),
                                primary: Color.fromARGB(255, 237, 28, 36),

                              ),

                              child: Text('Suivant',
                                style: TextStyle(
                                    fontSize: 17
                                ),),

                            ),
                          ),

                        )



                    ],

                  ),

                ],
              ),
            )));

  }
}