import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:projettest/Screens/Talent/ProfilMission/Cmission2.dart';
import 'E_connexion.dart';

class Cmission4 extends StatefulWidget {
  const Cmission4({super.key});

  @override
  State<Cmission4> createState() => _Cmission4State();
}

class _Cmission4State extends State<Cmission4> {
  @override
  bool check = false;
  bool isChecked = false;
  Widget build(BuildContext context) {
    return SafeArea(
        child:Scaffold(
          body: Padding (padding: EdgeInsets.all(25),
            child: Column (
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(

                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: Text(
                          " <",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,

                          ),
                        ),
                      ),
                      SizedBox(width:15,),


                      Expanded(
                          child:Container(
                            height: 5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Color.fromARGB(255, 228, 225, 225),
                            ),
                            child: LinearProgressIndicator(
                              value: 0.3,
                              backgroundColor: Colors.transparent,
                              valueColor: AlwaysStoppedAnimation<Color>(Colors.red[700]!),

                            ),
                          )
                      )



                    ],
                  ),


                  SizedBox(height: 20,),

                  Text("Pour quel métier\nsouhaitez-vous également\nrecevoir des missions",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),),

                  SizedBox(height: 30,),

                  SizedBox(height: 30,
                    child: ElevatedButton(
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                        onPrimary:Colors.white  ,
                        primary: Theme.of(context).dialogBackgroundColor,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(7),
                          side: BorderSide(
                            color: Color.fromARGB(255, 221, 220, 220),

                          ),
                        ),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.add, size: 15,),
                          SizedBox(width: 2),
                          Text('Plongeur(se)',
                            style: TextStyle(fontSize: 15),),

                        ],

                      ),
                    ),),

                  SizedBox(height: 10,),

                  SizedBox(height: 30,
                    child: ElevatedButton(
                      onPressed: () {},
                      style: ElevatedButton.styleFrom(
                        onPrimary:Colors.black  ,
                        primary: Theme.of(context).dialogBackgroundColor,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(7),
                          side: BorderSide(
                            color: Color.fromARGB(255, 221, 220, 220),

                          ),
                        ),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.add, size: 15,),
                          SizedBox(width: 2),
                          Text('Cuisinier(ière)',
                            style: TextStyle(fontSize: 15),),

                        ],

                      ),
                    ),),


                  SizedBox(height: 10,),

                  SizedBox(height: 30,
                    child: ElevatedButton(
                      onPressed: () {
                        setState(() {
                          isChecked = !isChecked;
                        });
                      },

                      style: ElevatedButton.styleFrom(
                        onPrimary:Colors.black  ,
                        primary: Theme.of(context).dialogBackgroundColor,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(
                            color: Color.fromARGB(255, 221, 220, 220),

                          ),
                        ),
                      ),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.add, size: 15,),
                          SizedBox(width: 2),
                          Text('Barman',
                            style: TextStyle(fontSize: 15),),

                        ],

                      ),
                    ),),
                  SizedBox(height: 10,),

                  Container(
                    decoration: BoxDecoration(
                      color: Colors.grey[200],
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          isChecked = !isChecked;
                        });
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(10),
                        child: isChecked
                            ? Icon(Icons.done, color: Colors.green)
                            : Icon(Icons.add, color: Colors.blue),
                      ),
                    ),
                  ),



                  SizedBox(height: 100,),



                  Center(

                    child:
                    Container
                      (height: 40,width:300,
                      child: ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              PageRouteBuilder(
                                //je teste juste que le redirection marche

                                  pageBuilder: (_, __, ___) => Cmission2()
                              )
                          );
                        },
                        style: ElevatedButton.styleFrom(
                          onPrimary:Colors.black  ,
                          primary: Theme.of(context).dialogBackgroundColor,
                          elevation: 0,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10),
                            side: BorderSide(
                              color: Color.fromARGB(255, 221, 220, 220),

                            ),
                          ),
                        ),
                        child: Text('Suivant',
                          style: TextStyle(fontSize: 15),),

                      ),
                    ),


                  )




                ]),),
        ));
  }
}