import 'package:flutter/material.dart';
import 'package:projettest/Screens/Situation.dart';
import '../../HomePage.dart';
import '../ProfilExperience/PosteOccupe.dart';

class NotExperience extends StatelessWidget {
  @override
  Color myColor = Color.fromRGBO(237, 28, 36, 1.0);
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
              children: [
                SizedBox(
                  height: 80.0,
                ),
                Image.asset("assets/image/entree-interdite.png",
                  height: 150,

                ),
                Text("Vous ne pouvez pas recevoir de\n mission pour le moment",
                    style: TextStyle(color: Colors.black,fontSize: 18,fontWeight: FontWeight.bold, )),
                SizedBox(height: 40),

                RichText(
                  text: TextSpan(
                    text: "* Avoir au mois 6 mois d'expérience",
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Text("* Avoir plus de 18 ans",
                    style: TextStyle(color: Colors.black,fontSize: 18)),
                SizedBox(height: 10),
                Text("* Être autorisé(e) à travailler dans votre pays",style: TextStyle(color: Colors.black,fontSize: 18)),
                SizedBox(height: 10),

                Padding(padding: EdgeInsets.only(top: 300)),
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(Colors.red[500]) ,
                  ),
                  onPressed: (){
                    Navigator.push(
                        context,
                        PageRouteBuilder(
                          //redirige sur la homepage pour que l'utilisateur qui na pas d'experience revient un autre jour

                            pageBuilder: (_, __, ___) => HomePage()
                        )
                    );
                  },
                  child: Text("Quitter",
                    style:  TextStyle(color: Colors.black,fontSize: 25)),),
              ]
          )
      ),
      backgroundColor: Colors.white,
    );
  }

}
