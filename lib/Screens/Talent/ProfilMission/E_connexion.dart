import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:projettest/Screens/Talent/ProfilMission/Cmission4.dart';

class E_connexion extends StatefulWidget {
  const E_connexion({super.key});
  @override
  State<E_connexion> createState() => _E_connexionState();
}

class _E_connexionState extends State<E_connexion> {
  @override
  Widget build(BuildContext context) {
    return  SafeArea(
        child: Scaffold(
          body: Padding(
            padding: EdgeInsets.only(top: 20, left: 10),
            child:Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                SizedBox(
                  height: 50,
                ),
                Text('Dans quelle ville souhaitez-vous\nobtenir des missions.',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 20,


                  ),),
                SizedBox(
                  height: 30,
                ),

                ElevatedButton(
                  onPressed: () {},

                  child: Text('Lyon'),
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).dialogBackgroundColor,
                    onPrimary: Colors.black,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: Color.fromARGB(255, 221, 220, 220),
                        width: 1,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),

                ElevatedButton(
                  onPressed: () {},

                  child: Text('Lille'),
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).dialogBackgroundColor,
                    onPrimary: Colors.black,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: Color.fromARGB(255, 221, 220, 220),
                        width: 1,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),

                ElevatedButton(
                  onPressed: () {},

                  child: Text('Grenoble'),
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).dialogBackgroundColor,
                    onPrimary: Colors.black,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: Color.fromARGB(255, 221, 220, 220),
                        width: 1,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),

                ElevatedButton(
                  onPressed: () {},

                  child: Text('Marseille'),
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).dialogBackgroundColor,
                    onPrimary: Colors.black,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: Color.fromARGB(255, 221, 220, 220),
                        width: 1,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () {},
                  child: Text('Paris'),
                  style: ElevatedButton.styleFrom(
                    primary: Theme.of(context).dialogBackgroundColor,
                    onPrimary: Colors.black,
                    elevation: 0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(
                        color: Color.fromARGB(255, 221, 220, 220),
                        width: 1,
                      ),
                    ),
                  ),
                ),
                Center(

                  child:
                  Container
                    (height: 40,width:300,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                            context,
                            PageRouteBuilder(
                              //je teste juste que le redirection marche

                                pageBuilder: (_, __, ___) =>Cmission4()
                            )
                        );
                      },
                      style: ElevatedButton.styleFrom(
                        onPrimary:Colors.black  ,
                        primary: Theme.of(context).dialogBackgroundColor,
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                          side: BorderSide(
                            color: Color.fromARGB(255, 221, 220, 220),

                          ),
                        ),
                      ),
                      child: Text('Suivant',
                        style: TextStyle(fontSize: 15),),

                    ),
                  ),


                )

              ],
            ) ,
          ),
        )
    );
  }
}