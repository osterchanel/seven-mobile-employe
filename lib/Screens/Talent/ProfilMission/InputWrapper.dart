import 'package:flutter/material.dart';
import 'package:projettest/Screens/Talent/ProfilMission/Profil1.dart';
import 'Cmission.dart';


class InputWrapper extends StatelessWidget {
  Color myColor = Color.fromRGBO(237, 28, 36, 1.0);
  Color myColorBody = Color.fromRGBO(253, 232, 233, 1.0);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myColorBody,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: myColor,
        leading: IconButton(
          onPressed: () {
            Navigator.push(
                context,
                PageRouteBuilder(
                    pageBuilder: (_, __, ___) => Profil1()
                )
            );
          },
          icon: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
      ),

        body:SingleChildScrollView(
        child: Container(
          child: Stack(
              children: [
           Column(
          children: [
            Image.asset("./assets/image/saven.png",height: 200,),
              Text("Creer votre profil",
              style: TextStyle(color: Colors.black,fontSize: 30,fontWeight: FontWeight.bold, )),
              SizedBox(height: 20),

          Row(
          children: [
            Padding(padding: EdgeInsets.symmetric(horizontal: 10)),
              Icon(
                Icons.arrow_forward,
                size: 30,
              ),
              Text('Preciser vos critere de mission',
                  style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),
              ),
          ],),

            SizedBox(height: 10),
          Row(
          children: [
            Padding(padding: EdgeInsets.symmetric(horizontal: 22)),
              Text("Renseignez vos experiences",
                  style:  TextStyle(color: Colors.black,fontSize: 18),
              ),
          ]),

            SizedBox(height: 10),

            Row(
                children: [
                  Padding(padding: EdgeInsets.symmetric(horizontal: 22)),
              Text("Completez vos informations",
                  style:  TextStyle(color: Colors.black,fontSize: 18)
              ),
                ]),

            SizedBox(height: 10),

            Row(
                children: [
                  Padding(padding: EdgeInsets.symmetric(horizontal: 22)),
              Text("Validez votre profil avec notre équipe!!",
                style:  TextStyle(color: Colors.black,fontSize: 18)
              ),
        ]),

            Padding(padding: EdgeInsets.only(top: 250)),

            ElevatedButton(
              style: ButtonStyle(
                backgroundColor: MaterialStatePropertyAll(myColor),
                padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 128, vertical: 10),),
              ),
              onPressed: (){
                Navigator.push(
                    context,
                    PageRouteBuilder(
                        pageBuilder: (_, __, ___) => Cmission()
                    )
                );
              },
              child: Text("Demarer",
                  style:  TextStyle(color: Colors.black,fontSize: 25)),

            ),
            ],
           ),
    ]
        ),

    ),
    ),
    );

  }
}
