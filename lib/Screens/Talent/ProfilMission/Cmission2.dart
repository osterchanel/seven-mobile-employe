import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:projettest/Screens/Talent/ProfilExperience/InputWrapperExp.dart';


class Cmission2 extends StatefulWidget {
  const Cmission2({super.key});

  @override
  State<Cmission2> createState() => _Cmission2State();
}

class _Cmission2State extends State<Cmission2> {
  @override
  List <bool>_isChecked = [false,false,false,false];
  bool isAnyChecked() {
    for (var i = 0; i < _isChecked.length; i++) {
      if (_isChecked[i]) {
        return true;
      }
    }
    return false;
  }

  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: Padding (padding: EdgeInsets.all(25),
              child: Column (
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(

                    children: [
                      GestureDetector(
                        onTap: () {},
                        child: Text(
                          " <",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 18,

                          ),
                        ),
                      ),
                      SizedBox(width:15,),


                      Expanded(
                          child:Container(
                            height: 5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: Color.fromARGB(255, 228, 225, 225),
                            ),
                            child: LinearProgressIndicator(
                              value: 0.45,
                              backgroundColor: Colors.transparent,
                              valueColor: AlwaysStoppedAnimation<Color>(Colors.orange),

                            ),
                          )
                      )



                    ],
                  ),


                  SizedBox(height: 20,),

                  Text("Combien de temps souhaitez-vous être\nen mission",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                    ),),

                  SizedBox(height: 40,),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Column(
                        children: [
                          Container(
                            height: 50,

                            decoration: BoxDecoration(
                              border: Border.all(color: Color.fromARGB(255, 218, 218, 218), width: 1.0),
                              borderRadius: BorderRadius.circular(10),

                            ),
                            child: Row(
                              children: [
                                Padding(padding: EdgeInsets.only(left: 10,)),
                                Checkbox(

                                  value: _isChecked[0],
                                  onChanged: (bool? value) {
                                    setState(() {
                                      _isChecked[0] = value!;
                                    });
                                  },

                                  activeColor: Colors.orange,
                                  shape: CircleBorder(),
                                ),

                                SizedBox(width : 7),


                                Text("Moins de 10 heures par semaines",
                                  style: TextStyle(

                                    fontSize: 15,
                                  ),)
                              ],
                            ),
                          )
                        ],
                      ) ,


                    ],

                  ),
                  SizedBox(height: 12,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Column(
                        children: [
                          Container(
                            height: 50,

                            decoration: BoxDecoration(
                              border: Border.all(color: Color.fromARGB(255, 218, 218, 218), width: 1.0),
                              borderRadius: BorderRadius.circular(10),

                            ),
                            child: Row(
                              children: [
                                Padding(padding: EdgeInsets.only(left: 10,)),
                                Checkbox(

                                  value: _isChecked[1],
                                  onChanged: (bool? value) {
                                    setState(() {
                                      _isChecked[1]= value!;
                                    });
                                  },

                                  activeColor: Colors.orange,
                                  shape: CircleBorder(),
                                ),

                                SizedBox(width : 7),


                                Text("Entre 10 et 20 heures par semaine",
                                  style: TextStyle(

                                    fontSize: 15,
                                  ),)
                              ],
                            ),
                          )
                        ],
                      ) ,


                    ],

                  ),
                  SizedBox(height: 12,),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Column(
                        children: [
                          Container(
                            height: 50,

                            decoration: BoxDecoration(
                              border: Border.all(color: Color.fromARGB(255, 218, 218, 218), width: 1.0),
                              borderRadius: BorderRadius.circular(10),

                            ),
                            child: Row(
                              children: [
                                Padding(padding: EdgeInsets.only(left: 10,)),
                                Checkbox(

                                  value: _isChecked[2],
                                  onChanged: (bool? value) {
                                    setState(() {
                                      _isChecked[2] = value!;
                                    });
                                  },

                                  activeColor: Colors.orange,
                                  shape: CircleBorder(),
                                ),

                                SizedBox(width : 7),


                                Text("Entre 20 et 40 heures par semaine",
                                  style: TextStyle(

                                    fontSize: 15,
                                  ),)
                              ],
                            ),
                          )
                        ],
                      ) ,


                    ],

                  ),
                  SizedBox(height: 12,),

                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Column(
                        children: [
                          Container(
                            height: 50,

                            decoration: BoxDecoration(
                              border: Border.all(color: Color.fromARGB(255, 218, 218, 218), width: 1.0),
                              borderRadius: BorderRadius.circular(10),

                            ),
                            child: Row(
                              children: [
                                Padding(padding: EdgeInsets.only(left: 10,)),
                                Checkbox(

                                  value: _isChecked[3],
                                  onChanged: (bool? value) {
                                    setState(() {
                                      _isChecked[3] = value!;
                                    });
                                  },

                                  activeColor: Colors.red[700],
                                  shape: CircleBorder(),
                                ),

                                SizedBox(width : 7),


                                Text("Plus de 40 heures par semaine",
                                  style: TextStyle(

                                    fontSize: 15,
                                  ),)
                              ],
                            ),
                          )
                        ],
                      ) ,



                    ],

                  ),

                  SizedBox(height: 150,),

                  if (isAnyChecked())
                    Center(


                      child:
                      Container(
                        width:300,
                        height: 55,

                        child: ElevatedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                PageRouteBuilder(
                                  //je teste juste que le redirection marche

                                    pageBuilder: (_, __, ___) => InputWrapperExp()
                                )
                            );
                          },
                          style: ElevatedButton.styleFrom(
                            elevation: 0,
                            padding: EdgeInsets.symmetric(vertical: 15),
                            primary: Color.fromARGB(255, 237, 28, 36),

                          ),

                          child: Text('Suivant',
                            style: TextStyle(
                                fontSize: 17
                            ),),

                        ),
                      ),

                    )




                ],
              ),
            )));

  }
}