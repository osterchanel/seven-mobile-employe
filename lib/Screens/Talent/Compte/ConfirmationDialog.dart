import 'package:flutter/material.dart';
import 'package:projettest/Screens/LoginPage.dart';
import '/MailSend.dart';
import 'package:flutter/src/material/button_style.dart';

class ConfirmationDialog extends StatefulWidget {
  @override

  _ConfirmationDialogState createState() => _ConfirmationDialogState();
}

class _ConfirmationDialogState extends State<ConfirmationDialog> {
  bool _isChecked = false;
  bool _isOver18Checked= false;
  bool _isCheckedconfid= false;
  Color myColor = Color.fromRGBO(237, 28, 36, 1.0);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: myColor,
        leading: IconButton(
          onPressed: () {
            Navigator.push(
                context,
                PageRouteBuilder(
                    pageBuilder: (_, __, ___) => LoginPage()
                )
            );
          },
          icon: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
      ),
      body:

      Column(

        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children:<Widget> [
          SizedBox(height: 10),
          Text("Avant d'aller plus loin",
            style: TextStyle(color: Colors.black,fontSize: 30,fontWeight: FontWeight.bold),),
          SizedBox(height: 20),
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                child:
                Checkbox(
                  activeColor: Colors.grey,
                    value: _isOver18Checked,
                    onChanged: (value) {
                      setState(() {
                        _isOver18Checked = value!;
                      }
                      );
                    }

                ),

              ),
              Text("Je declare avoir plus de 18 ans"),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                child:
                Checkbox(
                    activeColor: Colors.grey,
                    value: _isChecked,
                    onChanged: (value) {
                      setState(() {
                        _isChecked = value!;
                      });
                    }

                ),

              ),
              Text("J'accepte les conditions generales d'utilisation"),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 12),
                child:
                Checkbox(
                    activeColor: Colors.grey,
                    value: _isCheckedconfid,
                    onChanged: (value) {
                      setState(() {
                        _isCheckedconfid = value!;
                      });
                    }

                ),

              ),
              Text("J'accepte la politique de confidentialité de "
                  "SevenJobs"),
            ],
          ),

          SizedBox(height: 300),

          Row(

              children: [

                TextButton(
                  child: Text('Annuler',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w400, color: Colors.black)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },

                ),

                ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: myColor,
                ),
                  child: Text('Confirmer',style: TextStyle(fontSize: 20,fontWeight: FontWeight.w400, color: Colors.black)),
                  onPressed: _isChecked && _isOver18Checked && _isCheckedconfid? () {
                    // Faites ce que vous voulez si l'utilisateur a accepté les conditions.
                    Navigator.push(
                        context,
                        PageRouteBuilder(
                            pageBuilder: (_, __, ___) => MailSend()
                        )

                    );
                  }
                      : null,
                 
                )
              ]
          )
        ],
      ),
    );

  }
}
