import 'package:flutter/material.dart';

import '../../LoginPage.dart';

class InputWrapperExp2 extends StatelessWidget {
  @override
  Color myColor = Color.fromRGBO(237, 28, 36, 1.0);
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(



        title: Text(""), backgroundColor: myColor,
        actions: [
          PopupMenuButton<String>(
            itemBuilder: (BuildContext context) => <PopupMenuItem<String>>[
              PopupMenuItem<String>(
                value: 'option1',
                child: Text('Se déconnecter'),
              ),
              PopupMenuItem<String>(
                value: 'option2',
                child: Text('Supprimer mon compte'),
              ),

            ],
            onSelected: (String value) {
              //
              print('Option sélectionnée: $value');
              if (value == 'option1') {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );
              }
              else if(value == 'option2'){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );


              }

            },
            offset: Offset(0, 60),
          )



          /*  IconButton(



            icon: const Icon(Icons.more_vert), onPressed: () {
              PageRouteBuilder(
              pageBuilder: (_, __, ___) => LoginPage()
          ); },)
        */
        ],
        actionsIconTheme: IconThemeData(
          size:30,
        ),
      ),
      body: Center(
          child: Column(
              children: [
                Image.asset("assets/image/saven.png",
                  height: 200,
                ),
                Text("Creer votre profil",
                    style: TextStyle(color: Colors.black,fontSize: 30,fontWeight: FontWeight.bold, )),
                SizedBox(height: 20),
                Icon(
                  Icons.check_box_outlined,
                  size: 50,
                  color: Colors.green,
                ),
                Text("Preciser vos critere de mission",
                    style: TextStyle(color: Colors.black,fontSize: 15,fontWeight: FontWeight.bold, )),
                SizedBox(height: 10),
                Icon(
                  Icons.check_box_outlined,
                  size: 50,
                  color: Colors.green,
                ),
                Text("Renseignez vos experiences"),
                SizedBox(height: 10),
                Icon(
                  Icons.arrow_forward,
                  size: 50,
                ),
                Text("Completez vos informations"),
                SizedBox(height: 10),
                Text("Validez votre profil avec notre équipe!!"),
                Padding(padding: EdgeInsets.only(top: 300)),
                ElevatedButton.icon(

                  style:  ButtonStyle(

                    backgroundColor: MaterialStatePropertyAll(myColor) ,
                    padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 128, vertical: 10),),
                    iconSize: MaterialStateProperty.all(20),

                  ),
                  onPressed: (){

                  },
                  label: Text("Compléter"),

                  icon: Icon(Icons.start),

                ),

              ]

          )
      ),
      backgroundColor: Colors.white,
    );
  }

}
