import 'package:flutter/material.dart';

class LastExperience extends StatefulWidget {
  @override

  _LastExperienceState createState() => _LastExperienceState();
}

class _LastExperienceState extends State<LastExperience> {
  int _value = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body:
      Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children:<Widget> [
          SizedBox(height: 100),
          Text("De quand date votre dernière expérience en restauration?",
            style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),),
          SizedBox(height: 20),
          Row(
            children: [
              Radio(
                  value: 1,
                  groupValue:_value,
                  onChanged: (value){
                    setState(() {
                      _value = value!;
                    });
                  }
              ),
              SizedBox(width: 10.0,),
              Text("Je suis actuellement en poste"),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Radio(
                  value: 2,
                  groupValue:_value,
                  onChanged: (value){
                    setState(() {
                      _value = value!;
                    });
                  }
              ),
              SizedBox(width: 10.0,),
              Text("Moins de 6 mois"),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Radio(
                  value: 3,
                  groupValue:_value,
                  onChanged: (value){
                    setState(() {
                      _value = value!;
                    });
                  }
              ),
              SizedBox(width: 10.0,),
              Text("Plus de 6 mois"),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Radio(
                  value: 4,
                  groupValue:_value,
                  onChanged: (value){
                    setState(() {
                      _value = value!;
                    });
                  }
              ),
              SizedBox(width: 10.0,),
              Text("Plus de 3 ans"),
            ],
          ),
          SizedBox(height: 20),

          SizedBox(height: 200),

          Row(

              children: [
                ElevatedButton(
                    child:
                    Text('Continuer', style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),),
                    onPressed: (){
                      //
                      // PageRouteBuilder(
                      //     pageBuilder: (_, __, ___) => InputWrapperInfo()
                      // );
                    }
                )
              ]
          )
        ],
      ),
    );

  }
}

