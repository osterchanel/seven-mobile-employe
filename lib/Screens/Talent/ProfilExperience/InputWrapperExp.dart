import 'package:flutter/material.dart';
import '../../LoginPage.dart';
import 'PosteOccupe.dart';

class InputWrapperExp extends StatelessWidget {
  @override
  Color myColor = Color.fromRGBO(237, 28, 36, 1.0);
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(

        title: Text(""), backgroundColor: myColor,
        actions: [
          PopupMenuButton<String>(
            itemBuilder: (BuildContext context) => <PopupMenuItem<String>>[
              PopupMenuItem<String>(
                value: 'option1',
                child: Text('Se déconnecter'),
              ),
              PopupMenuItem<String>(
                value: 'option2',
                child: Text('Supprimer mon compte'),
              ),

            ],
            onSelected: (String value) {
              //
              print('Option sélectionnée: $value');
              if (value == 'option1') {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );
              }
                else if(value == 'option2'){
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => LoginPage()),
                );


              }

            },
            offset: Offset(0, 60),
          )
        ],
        actionsIconTheme: IconThemeData(
          size:30,
        ),
      ),
      body: Center(
          child: Column(
              children: [
                Image.asset("./assets/image/saven.png",height: 200,),
                Text("Creer votre profil",
                    style: TextStyle(color: Colors.black,fontSize: 30,fontWeight: FontWeight.bold, )),
                SizedBox(height: 20),

                Row(
                  children: [
                    Padding(padding: EdgeInsets.symmetric(horizontal: 10)),
                    Icon(
                      Icons.check_outlined,
                      size: 32,
                      color: Colors.green,
                    ),
                    Text('Preciser vos critere de mission',
                      style: TextStyle(color: Colors.black,fontSize: 18),
                    ),
                  ],),

                SizedBox(height: 10),

                Row(
                  children: [
                    Padding(padding: EdgeInsets.symmetric(horizontal: 10)),
                    Icon(
                      Icons.arrow_forward,
                      size: 30,
                    ),
                    Text('Renseignez vos experiences',
                      style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),
                    ),
                  ],),

                SizedBox(height: 10),

                Row(
                    children: [
                      Padding(padding: EdgeInsets.symmetric(horizontal: 22)),
                      Text("Completez vos informations",
                          style:  TextStyle(color: Colors.black,fontSize: 18)
                      ),
                    ]),

                SizedBox(height: 10),

                Row(
                    children: [
                      Padding(padding: EdgeInsets.symmetric(horizontal: 22)),
                      Text("Validez votre profil avec notre équipe!!",
                          style:  TextStyle(color: Colors.black,fontSize: 18)
                      ),
                    ]),

                Padding(padding: EdgeInsets.only(top: 250)),

                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStatePropertyAll(myColor),
                    padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 128, vertical: 10),),
                  ),
                  onPressed: (){
                    Navigator.push(
                        context,
                        PageRouteBuilder(
                            pageBuilder: (_, __, ___) => PosteOccupe()
                        )
                    );
                  },
                  child: Text("Demarer",
                      style:  TextStyle(color: Colors.black,fontSize: 25)),

                ),

              ]

          )
      ),
      backgroundColor: Colors.white,
    );
  }

}
