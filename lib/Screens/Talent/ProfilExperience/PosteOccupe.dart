import 'package:flutter/material.dart';
import 'LastExperience.dart';

class PosteOccupe extends StatefulWidget {
  @override

  _PosteOccupeState createState() => _PosteOccupeState();
}

class _PosteOccupeState extends State<PosteOccupe> {
  bool _isChecked = false;
  bool _isCheckedchef= false;
  bool _isCheckedserveur= false;
  bool _isCheckedrunner= false;
  bool _isCheckedpoly= false;
  @override
  Widget build(BuildContext context) {

    return Scaffold(

      body:

      Column(

        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children:<Widget> [
          SizedBox(height: 100),
          Text("Quel(s) poste(s) avez-vous occupé(s) en restauration?",
            style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),),
          SizedBox(height: 20),
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                child:
                Checkbox(
                    value: _isChecked,
                    onChanged: (value) {
                      setState(() {
                        _isChecked = value!;
                      }
                      );
                    }

                ),

              ),
              Text("Responsable de salles"),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                child:
                Checkbox(
                    value: _isCheckedchef,
                    onChanged: (value) {
                      setState(() {
                        _isCheckedchef = value!;
                      });
                    }

                ),

              ),
              Text("Chef de rang"),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                child:
                Checkbox(
                    value: _isCheckedserveur,
                    onChanged: (value) {
                      setState(() {
                        _isCheckedserveur = value!;
                      });
                    }

                ),

              ),
              Text("Serveur"),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                child:
                Checkbox(
                    value: _isCheckedrunner,
                    onChanged: (value) {
                      setState(() {
                        _isCheckedrunner = value!;
                      });
                    }

                ),

              ),
              Text("Runner"),
            ],
          ),
          SizedBox(height: 20),
          Row(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                child:
                Checkbox(
                    value: _isCheckedpoly,
                    onChanged: (value) {
                      setState(() {
                        _isCheckedpoly = value!;
                      });
                    }

                ),

              ),
              Text("Equipier polyvalent"),
            ],
          ),
          SizedBox(height: 200),

          Row(

              children: [
                ElevatedButton(
                  child: Text('Confirmer', style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.bold),),
                  onPressed: _isChecked  && _isCheckedchef? () {
                    // Faites ce que vous voulez si l'utilisateur a accepté les conditions.
                    Navigator.push(
                        context,
                        PageRouteBuilder(
                            pageBuilder: (_, __, ___) => LastExperience()
                        )

                    );
                  }
                      : null,

                )
              ]
          )
        ],
      ),
    );

  }
}
