import 'package:flutter/material.dart';
import 'LoginPage.dart';

class Connexion extends StatefulWidget {
  const Connexion({super.key});
  @override
  State<Connexion> createState() => _ConnexionState();
}

class _ConnexionState extends State<Connexion> {
  Color myColor = Color.fromRGBO(253, 232, 233, 1.0);
  Color myColorNavBar = Color.fromRGBO(237, 28, 36, 1.0);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: myColorNavBar,
        leading: IconButton(
          onPressed: () {
            Navigator.push(
                context,
                PageRouteBuilder(
                    pageBuilder: (_, __, ___) => LoginPage()
                )
            );
          },
          icon: const Icon(Icons.arrow_back_ios_new_sharp),
        ),
      ),
        backgroundColor: myColor,

    );
  }
}
