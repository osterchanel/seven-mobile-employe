import 'package:flutter/material.dart';
import 'Password_valide.dart';

class New_password extends StatefulWidget {
  const New_password({super.key});

  @override
  State<New_password> createState() => _New_passwordState();
}

class _New_passwordState extends State<New_password> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body:Padding (padding: EdgeInsets.all(20),
      child:Center(child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
        //Image(image: image)
        Text("Renseigez votre nouveau mot de passe",
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 18
        ),),
        SizedBox(height: 20,),

        Text("Nouveau mot de passe"),SizedBox(height: 2,),

        TextFormField(
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  hintText: 'Password',
                 
                  suffixIcon: Icon (Icons.visibility, color : Colors.grey),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),
                SizedBox(height: 15,),

        Text("Confirmez le nouveau mot de passe"),SizedBox(height: 2,),

        TextFormField(
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  hintText: 'Password',
                 
                  suffixIcon: Icon (Icons.visibility, color : Colors.grey),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),


                SizedBox(height: 50,),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                 children: [
                  ElevatedButton(onPressed: (){
                    Navigator.push(context, 
                    MaterialPageRoute(builder: (context)=> Password_valide()));
                  }, 
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(200, 40),
                    primary: Colors.orange
                  ),
                child: Text("Reinitialisez votre mot de passe",
                textAlign:TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                ),))
                 ],
                )

                


        ],
      )) ,) ,
    ));
  }
}