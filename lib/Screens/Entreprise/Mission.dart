import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:datetime_picker_formfield_new/datetime_picker_formfield.dart';
import 'package:geolocator/geolocator.dart';

class Mission extends StatefulWidget {
  const Mission({super.key});

  @override
  State<Mission> createState() => _MissionState();
}

class _MissionState extends State<Mission> {
  @override
  String location = '';
  void getCurrentLocation() async {
  Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high);
  setState(() {
    location = '${position.latitude}, ${position.longitude}';
  });
}
  final format = DateFormat("yyyy-MM-dd");
  Widget build(BuildContext context) {
   
   

    return SafeArea(child: Scaffold(
      appBar: AppBar(
        leading:IconButton(
                    icon: Icon(Icons.arrow_back_ios_new, size: 20.0,),
                    onPressed: () {
                       Navigator.of(context).pop();
                    },
                    color: Colors.white,
                  ) ,
        title: Text('Nouvelle mission',style: TextStyle(color: Colors.white,), ),
        backgroundColor: Colors.red,
        elevation: 0,
        centerTitle: true,
      ),
      
      
      
  

      body: Padding(padding: EdgeInsets.all(20),
      
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          
          Expanded(child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text("Votre besoin",style: TextStyle( fontWeight: FontWeight.bold, fontSize: 20),),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              Container(
                height: 20,
                width: 20,
                padding: EdgeInsets.only(top:1),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.black,
                ),
                child: Text("1", style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
              ),
              Text ("  A quelle addresse", style: TextStyle(fontSize: 15),),
            ],
          ),
  
          SizedBox(height: 7,),
          TextFormField(
            cursorColor: Colors.grey,
            style: TextStyle(color : Colors.black),
            decoration: InputDecoration(
              hintText: "Localisation",
               prefixIcon: Icon(Icons.location_on, color: Colors.black,),
             border : OutlineInputBorder(borderRadius: BorderRadius.circular(10), 
             borderSide : BorderSide(color: Colors.black)),

             focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(10), 
             borderSide : BorderSide(color: Colors.black))
            ),
                      ),
            TextField(
                  decoration: InputDecoration(
                    labelText: 'Localisation',
                  ),
                  controller: TextEditingController(text: location),
                  onChanged: (value) {
                    location = value;
                  },
                ),
                ElevatedButton(
                    onPressed: getCurrentLocation,
                    child: Text('Obtenir la localisation'),
                  ),


          SizedBox(height: 10,),
           
           Row(
            children: [
              Container(
                height: 20,
                width: 20,
                padding: EdgeInsets.only(top:1),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.black,
                ),
                child: Text("2", style: TextStyle(color: Colors.white),textAlign: TextAlign.center,),
              ),
              Text (" Pour quelle date", style: TextStyle(fontSize: 15),),
            ],
          ),
           SizedBox(height: 7,),
           
            DateTimeField(
        format: format,
        onShowPicker: (context, currentValue) {
          return showDatePicker(
            context: context,
            firstDate: DateTime(1900),
            initialDate: currentValue ?? DateTime.now(),
            lastDate: DateTime(2100),
            
          );
        },
        cursorColor: Colors.grey,
            style: TextStyle(color : Colors.black),
            decoration: InputDecoration(
              labelText: 'Date',
              labelStyle: TextStyle(color: Colors.grey),
              prefixIcon: Icon(Icons.calendar_month,color: Colors.black,),
             border : OutlineInputBorder(borderRadius: BorderRadius.circular(10), 
             borderSide : BorderSide(color: Colors.black)),

             focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(10), 
             borderSide : BorderSide(color: Colors.black))
            ),
      ),

            ],

          )),
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
            children: [

            Column(
              
              children: [
              
               IconButton(
              icon: Icon(Icons.add_box_rounded),
              onPressed: () {},
            ),
            SizedBox(height: 2,),
            Text("Nouvelle mission", style: TextStyle(fontSize: 15),)
            ] ),
            SizedBox(width: 15,),
             Column(children: [
               IconButton(
              icon: Icon(Icons.map),
              onPressed: () {},
            ),
            SizedBox(height: 2,),
            Text("Mission",style: TextStyle(fontSize: 15),)
            ] ),
            SizedBox(width: 15,),

             Column(children: [
               IconButton(
              icon: Icon(Icons.add_call),
              onPressed: () {},
            ),
            SizedBox(height: 2,),
            Text("Assistance",style: TextStyle(fontSize: 15),)
            ] ),
            SizedBox(width: 15,),

             Column(children: [
               IconButton(
              icon: Icon(Icons.menu),
              onPressed: () {},
            ),
            SizedBox(height: 2,),
            Text("Menu",style: TextStyle(fontSize: 15),)
            ] ),
            
          ],)
            ],


          ),
          
        ],
      ),
      
           ) 
   
    ));
  }
}