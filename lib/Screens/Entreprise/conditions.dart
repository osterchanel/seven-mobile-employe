import 'package:flutter/material.dart';
import 'Site1.dart';

class Condition extends StatefulWidget {
  const Condition({super.key});

  @override
  State<Condition> createState() => _ConditionState();
}

class _ConditionState extends State<Condition> {
  @override
  bool ischecked = false;
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(body:Padding (padding: EdgeInsets.all(30),

        child :Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),

             SizedBox(height: 10,),

             Center(
              
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 3,
                  width: 70,
                  color: Color.fromARGB(255, 246, 176, 72),
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Color.fromARGB(255, 246, 176, 72) ,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Color.fromARGB(255, 246, 176, 72) ,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                )
              ],
             ) ,
             ),

             


             SizedBox(height: 20,),

             Text("Nos conditions d'utilisation",
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),

           
             Expanded(
              child: Center(
                child: Column(
              children: [

                 Row(
                  children: [
                    Checkbox(value: ischecked, onChanged:(value){
                      setState(() {
                       ischecked = value!;
                      });
                    }
                    ),
                    Padding(padding: EdgeInsets.only(top: 54),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text("J'accepte les ",
                 style: TextStyle(fontSize: 17),),
                 InkWell(
                      onTap: () {
                      },
                      child: Text("Conditions Générales",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                          ],
                        ),
                        Row(
                  children: [
                 InkWell(
                      onTap: () {
                      },
                      child: Text("d'Utilisation de Seven JOB",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                    Text(", les ",
                 style: TextStyle(fontSize: 17),),

                 InkWell(
                      onTap: () {
                      },
                      child: Text("Conditions",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                  
                  ],
                 ),
                 Row(
                  children: [
                 InkWell(
                      onTap: () {
                      },
                      child: Text("Générales d'Utilisation de Stripe",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                    Text(", et les ",
                 style: TextStyle(fontSize: 17),),
                  ],
                 ),
                  Row(
                  children: [
                 InkWell(
                      onTap: () {
                      },
                      child: Text("Générales d'Utilisation de Stripe Connect",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                    
                  ],
                 ),
                      ],
                    ),)
                   
                  
                  ],
                 ),
                 SizedBox(
                  height: 10,
                 ),

                 Row(
                  children: [
                     Checkbox(value: ischecked, onChanged:(value){
                      setState(() {
                       ischecked = value!;
                      });
                    }
                    ),
               
                    
                    Padding(padding: EdgeInsets.only(top: 15),
                    child :Column(
                      
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text("J'accepte la ",
                 style: TextStyle(fontSize: 17),),
                 InkWell(
                      onTap: () {
                      },
                      child: Text("Politique de confidentialité",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                          ],
                        ),
                        Row(
                  children: [
                 InkWell(
                      onTap: () {
                      },
                      child: Text("de Seven JOB",
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                  
                  ],
                 ),
                 
      
                      ],
                    ), ),
                    
                   
                  
                  ],
                 ),
                  

              ],
             ),)),

             
                

                
                
                 Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder: (context)=> Site1()));
                        
                      }, 
                        style: ElevatedButton.styleFrom(
                        elevation: 0,
                       primary: Colors.orange,
                       minimumSize: Size(200, 40)
                      
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                )),
                    ],
                  ),
                 )


                
        
                
                
                   

               
                  

                  
                 
                   

          ],
        ) ),));
  }
}