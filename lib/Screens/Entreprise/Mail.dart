import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'mdp_connexion.dart';

class Mail extends StatefulWidget {
  const Mail({super.key});

  @override
  State<Mail> createState() => _MailState();
}

class _MailState extends State<Mail> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:Scaffold(
        body:Padding (padding: EdgeInsets.all(30),

        child :Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),

             


             SizedBox(height: 20,),

             Text('Votre adresse email',
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),

             SizedBox(height: 20,),
             Expanded(child: Center(child: Column(
              children: [
                TextFormField(
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  hintText: ' Email',
                  prefixIcon: Icon(Icons.email, color: Colors.grey,),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),
              ],
             ),)),

             
                

                
                
                 Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder:(context)=>mdp_connexion()));
                      }, 
                        style: ElevatedButton.styleFrom(
                        elevation: 0,
                       primary: Colors.orange,
                       minimumSize: Size(200, 40)
                      
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                )),
                    ],
                  ),
                 )
                   

          ],
        ) )
        ) );
  }
}