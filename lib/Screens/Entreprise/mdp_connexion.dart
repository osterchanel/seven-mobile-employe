import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'conditions.dart';
import 'mdpoublie.dart';

class mdp_connexion extends StatefulWidget {
  const mdp_connexion({super.key});

  @override
  State<mdp_connexion> createState() => _mdp_connexionState();
}

class _mdp_connexionState extends State<mdp_connexion> {
  @override

  Widget build(BuildContext context) {
   

    return  SafeArea(
      child:Scaffold(
        body: Padding(padding: EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            
             SizedBox(height: 20,),

             Expanded(child: Center( child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text('Votre mot de passe',
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),

             SizedBox(height: 20,),

             TextFormField(
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  hintText: 'Password',
                  prefixIcon: Icon(Icons.lock, color: Colors.grey,),
                  suffixIcon: Icon (Icons.visibility, color : Colors.grey),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),


              ],
             ))),
                 Center(
                  
                  child:Column(
                    
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      InkWell(
                      onTap: () {
                       Navigator.push(context, 
                       MaterialPageRoute(builder: (context)=>Forget()));
                       
                      },
                      child: Text("Vous aviez oublié votre mot de passe",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        color : Colors.orange,
                        fontSize:   15
                      ),),
                    ),
                    SizedBox(height: 10,),
                    
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder: (context)=>Condition()));
                      }, 
                      style: ElevatedButton.styleFrom(
                       elevation: 0,
                       primary: Colors.grey,
                       minimumSize: Size(200, 40)
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                ))
                    ],
                  ) ,
                 ),

                  
                 
                   

          ],
        ),),
        
        
      ) );
  }
}