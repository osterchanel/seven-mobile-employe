import 'package:flutter/material.dart';

import 'Mailenvoye.dart';

class Forget extends StatefulWidget {
  const Forget({super.key});

  @override
  State<Forget> createState() => _ForgetState();
}

class _ForgetState extends State<Forget> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconButton(onPressed:(){},
             icon: Icon(Icons.arrow_back,size: 20,)
             ),

             


             SizedBox(height: 10,),
             Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text('Vous aviez oublié votre mot de passe',
             style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
             ),),

             SizedBox(height: 10,),
             Text("Renseignez l'adresse email associée à votre\ncompte et nous vous enverrons un lien pour\nréinitialiser votre mot de passe.",
             style: TextStyle(color: Colors.grey, fontSize: 15),),

             SizedBox(height: 10,),

             TextFormField(
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  hintText: 'Email',
                  prefixIcon: Icon(Icons.mail, color: Colors.grey,),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),

              ],
             )),

             

               
               


        
                Column(
                    
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    
                    children: [
                      
                   
                    
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder: (context) => Mail_send()));
                      }, 
                      style: ElevatedButton.styleFrom(
                       elevation: 0,
                       primary: Colors.grey,
                       minimumSize: Size(200, 40)
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                ))
                    ],
                  ) ,
              

                  
                 
          ],
        ),),
      ));
  }
}