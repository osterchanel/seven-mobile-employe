import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:projettest/Screens/Entreprise/password.dart';

class Coordonnees extends StatefulWidget {
  const Coordonnees({super.key});

  @override
  State<Coordonnees> createState() => _CoordonneesState();
}

class _CoordonneesState extends State<Coordonnees> {
  bool isChecked = false;
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:Scaffold(
        body:Padding (padding: EdgeInsets.all(30),

        child :Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),

             SizedBox(height: 10,),

             Center(
              
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 3,
                  width: 70,
                  color: Color.fromARGB(255, 246, 176, 72),
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                )
              ],
             ) ,
             ),

             


             SizedBox(height: 20,),

             Text('Vos coordonnées',
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),

             SizedBox(height: 20,),
             Expanded(child: Center(child: Column(
              children: [
                TextFormField(
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  hintText: ' Email',
                  prefixIcon: Icon(Icons.email, color: Colors.grey,),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),

                 Row(
                  children: [
                 Checkbox(
                    value: isChecked,
                    onChanged: (value) {
                      setState(() {
                        isChecked = value!;
                      });
                    },
                    activeColor: Color.fromARGB(255, 235, 163, 54),
                   
                  ),
                  SizedBox(
                    width: 2,
                  ),
                  
                  Padding(padding: EdgeInsets.only(
                  top: 55,
                 ),
                 child: Text('Je souhaite recevoir des actualités du\nsecteur et les conseils de Seven JOB par\nemail.Je peux me désabonner à\ntout moment.',
                 style: TextStyle(fontSize: 15),),
                 ),
                  
                  ],
                 ),

              ],
             ),)),

             
                

                
                
                 Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder:(context)=>Password()));
                      }, 
                        style: ElevatedButton.styleFrom(
                        elevation: 0,
                       primary: Colors.orange,
                       minimumSize: Size(200, 40)
                      
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                )),
                    ],
                  ),
                 )


                
        
                
                
                   

               
                  

                  
                 
                   

          ],
        ) )
        ) );
  }
}