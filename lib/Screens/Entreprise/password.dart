import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'conditions.dart';

class Password extends StatefulWidget {
  const Password({super.key});

  @override
  State<Password> createState() => _PasswordState();
}

class _PasswordState extends State<Password> {
  @override

  Widget build(BuildContext context) {
   

    return  SafeArea(
      child:Scaffold(
        body: Padding(padding: EdgeInsets.all(30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            IconButton(onPressed:(){},
             icon: Icon(Icons.arrow_back,size: 20,)
             ),
             SizedBox(height: 10,),

             Center(
              
              child:Row(
                mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 3,
                  width: 70,
                  color: Color.fromARGB(255, 246, 176, 72),
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Color.fromARGB(255, 246, 176, 72),
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                ),
                SizedBox(width: 3,),
                Container(
                  height: 3,
                  width: 70,
                  color: Colors.grey,
                )
              ],
             ) ,
             ),

             


             SizedBox(height: 20,),
             Expanded(child: Center( child: Column(
              children: [
                Text('Votre mot de passe',
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),

             SizedBox(height: 20,),

             TextFormField(
              style: TextStyle( color: Colors.black),
              decoration: InputDecoration(
                  hintText: 'Password',
                  prefixIcon: Icon(Icons.lock, color: Colors.grey,),
                  suffixIcon: Icon (Icons.visibility, color : Colors.grey),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    
                    ),
                    focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10),
                    borderSide: BorderSide  (color: Colors.black,),
                    
                    ),

                    
                ),
                ),


              ],
             ))),
                 Center(
                  
                  child:Column(
                    
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                    
                      ElevatedButton(onPressed:(){
                        Navigator.push(context, 
                        MaterialPageRoute(builder: (context)=>Condition()));
                      }, 
                      style: ElevatedButton.styleFrom(
                       elevation: 0,
                       primary: Colors.grey,
                       minimumSize: Size(200, 40)
                      ),
                      
                child: Text('Suivant',
                       style: TextStyle(
                        fontSize: 15
                       ),
                ))
                    ],
                  ) ,
                 ),

                  
                 
                   

          ],
        ),),
        
        
      ) );
  }
}