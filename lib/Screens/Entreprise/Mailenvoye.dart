import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:open_mail_app/open_mail_app.dart';

class Mail_send extends StatefulWidget {
  const Mail_send({super.key});

  @override
  State<Mail_send> createState() => _Mail_sendState();
}

class _Mail_sendState extends State<Mail_send> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body: Padding(padding: EdgeInsets.all(20),
      
        child:  Column(
        children: [
         Expanded(
              child : Center(
                child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
            height: 50,
            width: 50,
             decoration: BoxDecoration(
            color: Colors.green,
            shape: BoxShape.circle, // Forme du conteneur (cercle)
          ),
          child: Icon(
            Icons.check,
            color: Colors.white,
          ),
          ),
          
          SizedBox(height: 15,),
          Text('Email envoyé!',
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
             ),),
             SizedBox(height: 10,),
             Text("Vous avez reçu un email avec un lien pour\nmodifier votre mot de passe. Pensez à vérifier\nvos spams si vous ne le trouvez pas.",
             style: TextStyle(color: Colors.grey),
             textAlign: TextAlign.center,
             ),
                ],
              )

              ),
              
            
          ),
          
                 Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    ElevatedButton(
                      onPressed: () async {
                        final Uri emailLaunchUri = Uri(
                          scheme: 'mailto',
                          path: 'adresse_email@example.com',
                        );
                        if (await canLaunch(emailLaunchUri.toString())) {
                          await launch(emailLaunchUri.toString());
                        } else {
                          throw 'Impossible d\'ouvrir l\'application de messagerie';
                        }
                      },
                        style: ElevatedButton.styleFrom(
                        elevation: 0,
                       primary: Theme.of(context).dialogBackgroundColor,
                       onPrimary: Colors.black,
                       minimumSize: Size(200, 40),
                       shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side: BorderSide(color: Colors.grey, width: 1),
                      ),
                                ),
                      
                child: Text('Vérifiez vos mails',
                       style: TextStyle(
                        fontSize: 15
                       ),
                )),


                SizedBox(height: 10,),


                ElevatedButton(onPressed:(){}, 
                        style: ElevatedButton.styleFrom(
                        elevation: 0,
                       primary: Colors.orange,
                       minimumSize: Size(200, 40)
                      ),
                      
                child: Text('Vérifiez vos mails',
                       style: TextStyle(
                        fontSize: 15
                       ),
                )),


                  ],
                 ),




                 

                             
           
         

        ],
      ),
      
       ),
    ));
  }
}
