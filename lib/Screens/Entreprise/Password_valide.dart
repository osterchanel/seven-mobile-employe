import 'package:flutter/material.dart';
import 'connexion.dart';

class Password_valide extends StatefulWidget {
  const Password_valide({super.key});

  @override
  State<Password_valide> createState() => _Password_valideState();
}

class _Password_valideState extends State<Password_valide> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body: Padding(padding: EdgeInsets.all(10),
      child: Center(child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          //Image
          Image.network("https://thumbs.gfycat.com/VariablePoliticalBurro-max-1mb.gif",
          height: 100,
          width:  100,),
          SizedBox(height: 50,),
          Text("Votre mot de passe  a été modifié.",
          style: TextStyle(
            fontSize: 17,
          ),),
          SizedBox(height: 10,),
          Text("Vous pouvez accéder à Seven JOB en utilisant un nouveau mot de passe", textAlign: TextAlign.center,),
          SizedBox(height: 15,),
          Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                 children: [
                  ElevatedButton(onPressed: (){
                    Navigator.push(context, 
                    MaterialPageRoute(builder:(context)=>Connect()));
                    
                  }, 
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(200, 40),
                    primary: Colors.orange
                  ),
                child: Text("Retournez à la connexion",
                textAlign:TextAlign.center,
                style: TextStyle(
                  fontSize: 15,
                ),))
                 ],
                )





        ],
      )),),
    ));
  }
}