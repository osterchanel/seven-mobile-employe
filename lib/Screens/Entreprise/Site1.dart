import 'dart:math';
import 'package:flutter/material.dart';

import 'Site2.dart';

class Site1 extends StatefulWidget {
  const Site1({super.key});

  @override
  State<Site1> createState() => _Site1State();
}

class _Site1State extends State<Site1> {
  @override
  List <bool> isChecked = [false,false];
  bool isAnyChecked() {
      for (var i = 0; i < isChecked.length; i++) {
        if (isChecked[i]) {
          return true;
        }
      }
      return false;
    }
  Widget build(BuildContext context) {
   
   

    return SafeArea(child: Scaffold(
      appBar: AppBar(
        title: Text('Créer son site',style: TextStyle(color: Colors.black), ),
        backgroundColor: Theme.of(context).dialogBackgroundColor,
        elevation: 0,
         iconTheme: IconThemeData(color: Colors.black),
        
  leading: IconButton(
    icon: Icon(Icons.person),
    onPressed: () {},
  ),
  actions: [
    IconButton(
      icon: Icon(Icons.exit_to_app),
      onPressed: () {},
    ),
  ],
  centerTitle: true,
      ),
      body: Padding(padding: EdgeInsets.all(20),
      
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Text("Créer votre site sur Seven JOB",style: TextStyle( fontWeight: FontWeight.bold, fontSize: 20),),
          SizedBox(
            height: 20,
          ),
          Text ("Nom du site"),
          SizedBox(height: 7,),
          TextFormField(
            style: TextStyle(color : Colors.black),
            decoration: InputDecoration(
             border : OutlineInputBorder(borderRadius: BorderRadius.circular(10), 
             borderSide : BorderSide(color: Colors.black)),

             focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(10), 
             borderSide : BorderSide(color: Colors.black))
            ),
                      ),

          SizedBox(height: 10,),
           Text("Adresse principale"),
           SizedBox(height: 7,),
           TextFormField(
            style: TextStyle(color : Colors.black),
            decoration: InputDecoration(
             border : OutlineInputBorder(borderRadius: BorderRadius.circular(10), 
             borderSide : BorderSide(color: Colors.black)),

             focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(10), 
             borderSide : BorderSide(color: Colors.black))
            ),
                      ),
             SizedBox(height: 10,),
             Text("Vous pourriez ajouter des adresses plus tard.",textAlign:TextAlign.center,),

             SizedBox(height: 15,),
             Text("Nature de votre établissement", style: TextStyle( fontWeight: FontWeight.bold),),

             SizedBox(height: 10,),
             
           Row(
              children: [
                Checkbox(value: isChecked[0],
                          onChanged: (bool? value){
                  setState(() {
                    isChecked[0] = value!;
                  });
                },
                shape: CircleBorder(),
                activeColor:Colors.orange,), 
                
                Text("Restauration - Hotellerie")
                
              ],
             ),
            
             Row(
              children: [
                Checkbox(value: isChecked[1], onChanged:(bool? value){
                  setState(() {
                    isChecked[1] = value! ;
                  },
                );
                },
                shape: CircleBorder(),
                activeColor:Colors.orange,), 
                Text("Sanitaire - Medico-Social")
                
              ],
             ),

            ],
          )),
          ElevatedButton(
              onPressed: () {
                Navigator.push(context,
                 MaterialPageRoute(builder:(context)=>Site2()));
              },
              style: ElevatedButton.styleFrom(
                elevation: 0,
                padding: EdgeInsets.symmetric(vertical: 15),
                primary: Color.fromARGB(255, 230, 154, 39),
                
              ),

              child: Text('Créer mon site',
              style: TextStyle(
                fontSize: 17
              ),),
              
               ),
         
          

       
            

             





        ],
      ),
      
           )  
    ));
  }
}