import 'package:flutter/material.dart';

class Site2 extends StatefulWidget {
  const Site2({super.key});

  @override
  State<Site2> createState() => _Site2State();
}

class _Site2State extends State<Site2> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(child: Scaffold(
      body :Padding(padding: EdgeInsets.all(20),
      
        child:  Column(
        children: [
         Expanded(
              child : Center(
                child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.network("https://cdni.iconscout.com/illustration/premium/thumb/notification-on-smartphone-2725341-2261059.png",height: 200,width: 200,),
                  
          
          SizedBox(height: 25,),

          Text('Voulez-vous être informé(e)\nen temps réel ?',textAlign: TextAlign.center,
             style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.bold,
              
              
             ),),
             SizedBox(height: 10,),
             Text("Vous recevrez des notifications uniquement quand\nc'est utile: quand une mission est acceptée etc.\nPas de spam, promis.",
             style: TextStyle(color: Colors.grey),
             textAlign: TextAlign.center,
             ),
                ],
              )

              ),
              
            
          ),
          
                 Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                   ElevatedButton(onPressed:(){}, 
                        style: ElevatedButton.styleFrom(
                        elevation: 0,
                       primary: Colors.orange,
                       minimumSize: Size(200, 40)
                      ),
                      
                child: Text('Activez les notifications',
                       style: TextStyle(
                        fontSize: 15
                       ),
                )),

                 SizedBox(height: 10,),

                ElevatedButton(
                      onPressed: () {
                       
                      },
                        style: ElevatedButton.styleFrom(
                        elevation: 0,
                       primary: Theme.of(context).dialogBackgroundColor,
                       onPrimary: Colors.black,
                       minimumSize: Size(200, 40),
                       shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side: BorderSide(color: Colors.grey, width: 1),
                      ),
                                ),
                      
                child: Text('Plus tard',
                       style: TextStyle(
                        fontSize: 15
                       ),
                )),


                  ],
                 ),

                             
           
         

        ],
      ),
       ),
    ));
  }
}