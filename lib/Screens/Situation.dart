import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'Talent/ProfilMission/E_connexion.dart';

class Situation extends StatefulWidget {
  const Situation({super.key});

  @override
  State<Situation> createState() => _SituationState();
}

class _SituationState extends State<Situation> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
          body: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(padding: EdgeInsets.symmetric(vertical: 5,
                    horizontal: 5)),
                SizedBox(
                  height: 100,
                ),
                Image.asset("assets/image/saven.png",
                  height: 150,
                ),
//-----------------------------------------------------





                RichText(
                  text: TextSpan(
                  text: 'Votre situation a changé?\n',
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,

                  ),
                ),
                ),
               /* RichText(
                  text: TextSpan(
                    text: 'Texte à rayer',
                    style: TextStyle(
                      color: Colors.black,
                      decoration: TextDecoration.lineThrough,
                      decorationThickness: 2.0, // Épaisseur du trait de rature
                      decorationColor: Colors.red, // Couleur du trait de rature
                    ),
                  ),
                ),*/
//-----------------------------------------------------------------
                RichText(

                  text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(
                        text: 'Si vous souhaitez mettre à jour votre profil,\n',
                        style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),
                      TextSpan(
                        text: 'vous devez d abord le rénitialiser\n',
                        style: TextStyle(

                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),

                    ],

                  ),
                  textAlign: TextAlign.center,
                ),
                      RichText(
                      text: TextSpan(
                        text: 'Un rendez vous telephonique vous sera proposer par la suite avec notre équipe pour valider vos experience\n',
                        style: TextStyle(

                          fontSize: 16,
                          color: Colors.black,
                        ),
                      ),

                  textAlign: TextAlign.center,
                      ),
                  // Alignement global du RichText

//-----------------------------------------------------------------------
                SizedBox(
                  height: 330,
                ),
                Container(
                  width: 410,
                  height: 55,
                  child:ElevatedButton(

                    style:  ButtonStyle(

                      backgroundColor: MaterialStatePropertyAll(Colors.pink) ,
                      padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 128, vertical: 10),),
                      iconSize: MaterialStateProperty.all(20),

                    ),
                    onPressed: (){
                      Navigator.push(
                          context,
                          PageRouteBuilder(
                              pageBuilder: (_, __, ___) => E_connexion()
                          )
                      );
                    },
                    child: Text("Rénitialiser mon profil"),


                  ),
                ),
              ],
            ),
          ),
        )
    );
  }
}
