import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/services.dart';
import 'Entreprise/connexion.dart';
import 'LoginPage.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Color myColor = Color.fromRGBO(253, 232, 233, 1.0);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: myColor,
        body: Padding (padding: EdgeInsets.symmetric(horizontal: 30),
          child:Center(
            child:Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 40,vertical: 30),),
                  Image.asset("assets/image/saven.png",
                    height: 250,
                  ),
                  SizedBox(
                    height: 150,
                  ),
                  ElevatedButton(onPressed: (){
                    Navigator.push(
                        context,
                        PageRouteBuilder(
                            pageBuilder: (_, __, ___) => LoginPage()
                        )
                    );
                  },
                    style: ElevatedButton.styleFrom(
                        primary: Colors.white,
                        onPrimary: Colors.black,
                        shape: null,
                        padding:  EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                        textStyle:
                        const TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                    child: Text("App Talent",),
                  ),
                  SizedBox(
                    height: 80,
                  ),

                  ElevatedButton(onPressed: (){
                    Navigator.push(
                        context,
                        PageRouteBuilder(
                          //je teste juste que le redirection marche

                            pageBuilder: (_, __, ___) => Connect()
                        )
                    );
                  },
                      style: ElevatedButton.styleFrom(
                          primary: Colors.white,
                          onPrimary: Colors.black,
                          shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.grey, width: 0.1)
                          ),
                          padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                          textStyle:
                          const TextStyle(fontSize: 30, fontWeight: FontWeight.bold)),
                      child: Text("App Entreprise")
                  ),
                ]),
          ) ,)
    );
  }
  }
