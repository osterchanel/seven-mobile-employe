import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_switch/flutter_switch.dart';

import 'Talent/Compte/ConfirmationDialog.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);
  @override
  // void initState(){
  //   super.initState();
  //   this.domaines.add("id":1, "label": "Restaurant"});
  // this.domaines.add("id":2, "label": "Autres"});
// }
  State<LoginPage> createState() => _LoginPageState();
}


class _LoginPageState extends State<LoginPage> {
  TextEditingController controller = new TextEditingController();
  List<String> domaineSizesList = ["Restauration", "Santé", "Plaquiste","Autre"];
  bool displaydomaineSizesList = false;
  String? selectedDomaine;
  void _showDomaineDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text("Choisir un domaine"),
          content: SingleChildScrollView(
            child: ListBody(
              children: domaineSizesList.map((domaine) {
                return GestureDetector(
                  onTap: () {
                    setState(() {
                      selectedDomaine = domaine;
                      controller.text = selectedDomaine!;
                      Navigator.pop(context); // Ferme la boîte de dialogue
                    });
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(domaine),
                  ),
                );
              }
              )
                  .toList(),
            ),
          ),
        );
      },
    );
  }


  // List<dynamic> domaines = [];
  // String? domaineId;
  @override
  //declaration des variables
  bool hide = true;
  bool status = false;
  Color myColor = Color.fromRGBO(237, 28, 36, 1.0);
  Color myColorBody = Color.fromRGBO(253, 232, 233, 1.0);

  //final _domaineSizesList = ["Restauration", "Autre"];
  //String? _selectedValue = "";
  @override

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: myColor,
      body:SingleChildScrollView(
        child: Stack(
          children: [
            Container(
              margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.00),
              width: double.infinity,
              height: 590 ,
              decoration: BoxDecoration(
                  color: myColorBody,
                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(50),bottomLeft: Radius.circular(50))
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 40, vertical: 20),),
                  Text("Se connecter",style: TextStyle(fontSize: 30,fontWeight: FontWeight.w400),),

                  SizedBox(height: 10,),

                  ElevatedButton(
                    onPressed: _showDomaineDialog,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(selectedDomaine ?? "Sélectionner un domaine"),
                        Icon(Icons.arrow_drop_down),
                      ],
                    ),
                  ),




                  SizedBox(height: 35,),
                  TextField(
                    decoration: InputDecoration(
                      hintText: "Identifiant",
                    ),

                  ),
                  SizedBox(height: 35,),
                  TextField(
                    obscureText: hide,
                    decoration: InputDecoration(
                        hintText: "Mot de passe",
                        suffixIcon:IconButton(onPressed: (){
                          setState(() {
                            hide = !hide;
                          });
                        },icon:hide?Icon(Icons.visibility_off):Icon(Icons.visibility),)
                    ),
                  ),
                  Align(
                    alignment: Alignment.centerRight,
                    child: TextButton(onPressed: (){},child: Text("Mot de passe oublié?"),),
                  ),
                  SizedBox(height: 35,),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                          child: Text(
                            "Garder mes données",
                            style: TextStyle(color: Colors.black.withOpacity(0.6),fontWeight: FontWeight.bold,fontSize: 16),

                          ),
                        ),

                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 65,vertical: 17),
                          child: FlutterSwitch(

                              width: 60.0,
                              height: 25.0,
                              valueFontSize: 2.0,
                              toggleSize: 10.0,
                              value: status,
                              borderRadius: 10.0,
                              padding: 9.0,
                              showOnOff: true,
                              onToggle: (val) {
                                setState( () {
                                  status = val;
                                }
                                );
                              }
                          ),
                        ),
                      ]
                  ),

                  SizedBox(height: 15,),


                  ElevatedButton.icon(

                    style:  ButtonStyle(

                      backgroundColor: MaterialStatePropertyAll(myColor) ,
                      padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 128, vertical: 10),),
                      iconSize: MaterialStateProperty.all(20),

                    ),
                    onPressed: (){
                      // Navigator.push(
                      //     // context,
                      //     // PageRouteBuilder(
                      //     //   //je teste juste que le redirection marche
                      //     //
                      //     //     // pageBuilder: (_, __, ___) => Connexion()
                      //     // )
                      // );
                    },
                    label: Text("Connexion"),

                    icon: Icon(Icons.check),

                  ),


                ],
              ),
            ),
            Container(
                padding: EdgeInsets.symmetric(horizontal: 40,vertical: 10),
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.7),
                width: double.infinity,
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                      Text("Pas encore de compte chez nous?",style: TextStyle(color: Colors.black,fontSize: 30,fontWeight: FontWeight.w300),),
                      Text("Inscris toi et trouve des offres d'emplois",style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.w300),),
                      SizedBox(height: 10,),
                      ElevatedButton.icon(
                        style: ButtonStyle(
                          backgroundColor: MaterialStatePropertyAll(Colors.white) ,
                          padding: MaterialStatePropertyAll(EdgeInsets.symmetric(horizontal: 120, vertical: 20),),
                        ),
                        onPressed: (){
                          Navigator.push(
                              context,
                              PageRouteBuilder(
                                //je teste juste que le redirection marche

                                  pageBuilder: (_, __, ___) => ConfirmationDialog()
                              )
                          );
                        },
                        label: Text("S'incrire",
                            style:  TextStyle(color: Colors.black,fontSize: 17)),
                        icon: Icon(Icons.assignment_ind,color: Colors.black, size: 20,),
                      ),

                    ]
                )
            ),
          ],
        ),
      ),
    );
  }
}
